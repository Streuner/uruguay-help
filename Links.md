# Полезные ссылки

[Оригинальный пост в Telegram](https://t.me/russianinuruguay/219)

- Декларация которую надо заполнить в течение 48 часов до въезда в Уругвай.
  - https://declaracionjurada.ingresoauruguay.gub.uy/ 

- Местное Avito
  - https://mercadolibre.com.uy/

- Местные кочующие рынки
  - https://montevideo.gub.uy/mapas/ferias
  - https://ferias.uy/?departmentId=10&page=2
 
  Выбирайте район и увидите расписание. Очень советую, продукты дешевле (большая часть), качество и свежесть запредельное.

- Аналог IKEA или Леруа
  - https://sodimac.com.uy/  

  Именно аналог, сравнивать не стоит.

- Супермаркеты
  - https://geant.com.uy
  - https://tiendainglesa.com.uy
  - https://www.devoto.com.uy
  - https://www.disco.com.uy/
  - https://www.elgranero.com.uy
  - https://www.niter.com.uy
  - https://www.bocata.com.uy

- Поиск недвижимости
  - https://dueñodirecto.com.uy
  - http://inmuebles.com.uy/
  - https://www.remax.com.uy/
  - https://www.gallito.com.uy
  - https://www.infocasas.com.uy
  - https://www.buscandocasa.com
  - https://casasweb.com/

- Торги по спец. технике
  - https://www.uruguay-mop.com/subastas?p=2&filtersForm=1

- Все Министерства Уругвая со ссылками
  - https://www.gub.uy/organismos/term/2

- Регистро Сивиль - ЗАГС
  - www.mec.gub.uy

- Миграсьон
  - www.minterior.gub.uy

- Муниципалитет Монтевидео
  - www.montevideo.gub.uy

- Платёжные сервисы
  - https://www.abitab.com.uy/
  - https://www.redpagos.com.uy/#/

  Почти все оплаты через них. Счетчики, билеты, услуги и прочее

- Тачки здесь
  - https://autos.trovit.com.uy/
  - https://www.shoppingdeusados.com.uy/

- Междугородний автовокзал
  - https://www.trescruces.com.uy/

- Связь и интернет
  - https://claro.com.uy/personas/
    
    Самая дешевая мобильная связь и интернет
  - https://www.movistar.es/ 

- Гос. сан служба
  - http://www.ose.com.uy/clientes/contactenos

- Коммунальные платежи
  - https://portal.ute.com.uy/

  Здесь же можете выбрать тариф по коммуналке.

- Запись на бесплатные курсы по Испанскому языку
  - https://docs.google.com/forms/d/e/1FAIpQLSel4uyzfX8g8DhF0XWlQu3Vscu-WNDzwXNUeedKG2SgWNb2RA/viewform
  - eleinmigra@gmail.com 
  
  2 раза в неделю по 1,5 часа на протяжении 3,5 месяцев. Писать на английском или испанском.

- Лучшее приложения для изучения языков
  - https://ru.duolingo.com/

- Спортивные билеты и развлечения
  - https://redtickets.uy/
  - https://tickantel.com.uy/inicio/?2
  - https://www.urubasket.com/
  - https://fibalivestats.dcd.shared.geniussports.com/u/FUBB/2103140/
  - https://el.soccerway.com/

- Местная газета
  - https://www.instagram.com/observadoruy/

- Местные новости
  - https://www.elpais.com.uy/informacion/sociedad

- Медицинские услуги
  - https://www.asesp.com.uy/home

- Банк
  - [Itaú](https://www.itau.com.uy/)

    Счёт [Cuenta Pocket](https://www.itau.com.uy/inst/cuentapocket.html?utm_source=LandingDebito&utm_medium=LandingDebitoPocket&utm_campaign=LandingDebitoPocket&utm_content=LandingDebitoPocket) в Itaú можно открыть без подтверждения домашнего адреса
  - [BROU](https://www.brou.com.uy/)
    
    Местный Сбер

- Оформление карты платежной системы Прекс
    - https://www.prexcard.com/

- В разделах vacunacion места, где можно сделать прививки.
  - http://www.montevideo.gub.uy/institucional/noticias/vacunan-en-policlinicas-municipales
  - http://casmu.com.uy/centros-de-vacunacion/
  - http://www.asesp.com.uy/Servicios/Horarios-y-telefonos-de-Interes-uc34
  - http://www.medicauruguaya.com.uy/auc.aspx?21,13

- Негосударственный институт
  - https://um.edu.uy/

- Отдел начального образования
  - www.ceip.edu.uy

- Отдел среднего образования
  - www.ces.edu.uy

- Гос. система здравоохранения
  - www.asse.com.uy

- Гос. фонд соц. страхования
  - www.bps.gub.uy

- Гос. услуги
  - https://portal.gub.uy
  - https://extranjeros.gub.uy

    Гос. услуги для иностранцев

- Налоговая инспекция
  - www.dgi.gub.uy

- Кортээлектораль
  - www.corteelectoral.gub.uy

- Местныe интернет-магазины 
  - https://loi.com.uy/
  - https://electroventas.com.uy/

- Местные интернет-магазины компьютерной техники (и еще чего-то заодно)
  - https://www.zonatecno.com.uy/
  - https://www.pccompu.com.uy/
  - https://thotcomputacion.com.uy/
  - https://laaca.com.uy/

- Местные фин. организации выдающие гарантии для долгосрочной аренды
  - www.anda.com.uy
  - www.portoseguro.com.uy
  - www.anv.com.uy
