Службы доставки продуктов и блюд на дом: **Rappi**, **Pedidos Ya**. Доступны в мобильных сторах  

**Гирос, русские владельцы**  

Gyros, The Original  
21 de Setiembre 2459, 11300 Montevideo, Departamento de Montevideo 

**Крафтовое пиво**

Oso Pardo  
Obligado 1317, 11300 Montevideo, Departamento de Montevideo

Criaturas Nocturnas  
Av. Gral. Rivera 2130, 11200 Montevideo, Departamento de Montevideo

**Пицца + крафтовое пиво**
  
Malafama  
Maldonado 1970, 11200 Montevideo, Departamento de Montevideo

**Пицца**

Sbarro   
Nuevocentro Shopping, Departamento de Montevideo (есть в других ТЦ) 

Don Ciccio  
Bonpland 507, 11300 Montevideo, Departamento de Montevideo  

**Суши**  

Moshi Moshi  
F. García Cortinas 2446, 11300 Montevideo, Departamento de Montevideo  

Sumi Sushi  
Patria 699, 11300 Montevideo, Departamento de Montevideo  

**Плов**

Casa de pilaf  
Francisco Aguilar 772, 11300 Montevideo, Departamento de Montevideo

**Армянский ресторан**

Erevan  
José Ellauri 1308, 11300 Montevideo, Departamento de Montevideo
